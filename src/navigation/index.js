import React, {createContext, useState} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {
  SplashScreen,
  WelcomeScreen,
  HomeScreen,
  AboutScreen,
  AccountScreen,
  LoginScreen,
  InfoScreen,
  RegisterScreen,
} from '../screens';
import {Positif} from '../api';

const Stack = createStackNavigator();
export const RootContext = createContext();

const Router = () => {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [isError, setIsError] = useState(false);

  const [globalPos, setGlobalPos] = useState({});
  const [globalRec, setGlobalRec] = useState({});
  const [globalDeath, setGlobalDeath] = useState({});

  const [globalData, setGlobalData] = useState([]);

  const [indo, setIndo] = useState([]);
  const [provinsi, setProvinsi] = useState([]);

  const handleNameChange = (textUser) => {
    // return alert('name');
    setName(textUser);
    console.log({name});
  };

  const handleEmailChange = (textEmail) => {
    // return alert('email');
    setEmail(textEmail);
    console.log({email});
  };

  const handlePasswordChange = (textPassword) => {
    // return alert('passowrd');
    setPassword(textPassword);
    console.log({password});
  };

  const handleConfirmPasswordChange = (textConfirmPassword) => {
    // return alert('confirm passowrd');
    setConfirmPassword(textConfirmPassword);
    console.log({confirmPassword});
  };

  return (
    <RootContext.Provider
      value={{
        name,
        email,
        password,
        confirmPassword,
        handleNameChange,
        handleEmailChange,
        handlePasswordChange,
        handleConfirmPasswordChange,
      }}>
      <Stack.Navigator initialRouteName="Splash">
        <Stack.Screen
          name="Splash"
          component={SplashScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Welcome"
          component={WelcomeScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Login"
          component={LoginScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Home"
          component={HomeScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Account"
          component={AccountScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="About"
          component={AboutScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Info"
          component={InfoScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Register"
          component={RegisterScreen}
          options={{headerShown: false}}
        />
      </Stack.Navigator>
    </RootContext.Provider>
  );
};

export default Router;

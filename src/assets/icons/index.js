import LogoApp from './LogoApp.png';

import Login from './Login.png';
import Covid from './Covid.png';
import Cegah from './Cegah.png';
import Ciri from './Ciri.png';
import Video from './Video.png';
import Info from './Info.png';
import About from './About.png';

import Photo from './Photo.png';
import Facebook from './Facebook.png';
import Instagram from './Instagram.png';
import Twitter from './Twitter.png';
import Gitlab from './Gitlab.png';

export {
  LogoApp,
  Login,
  Covid,
  Cegah,
  Ciri,
  Video,
  Info,
  About,
  Photo,
  Facebook,
  Instagram,
  Twitter,
  Gitlab,
};

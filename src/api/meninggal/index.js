import React, {useState, useEffect} from 'react';
import {ActivityIndicator, View, Text} from 'react-native';
import Axios from 'axios';

const Death = () => {
  const [death, setDeath] = useState(0);
  const [isLoading, setisLoading] = useState(true);
  const [isError, setisError] = useState(false);

  useEffect(() => {
    //fetch data
    Axios.get('https://api.kawalcorona.com/meninggal')
      .then((res) => {
        // console.log(res);
        setDeath(res.data.value);
        setisLoading(false);
        setisError(false);
      })
      .catch((err) => {
        console.log(err);
        setisLoading(false);
        setisError(true);
      });
  });

  if (isLoading) {
    return (
      <View style={{alignItems: 'center', justifyContent: 'center', flex: 1}}>
        <ActivityIndicator size="large" color="red" />
      </View>
    );
  } else if (isError) {
    return (
      <View style={{alignItems: 'center', justifyContent: 'center', flex: 1}}>
        <Text>Load Error!</Text>
      </View>
    );
  }

  return <Text>{death}</Text>;
};
export default Death;

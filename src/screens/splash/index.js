import React, {useEffect} from 'react';
import {StyleSheet, View, Text, Image, Dimensions} from 'react-native';
import {LogoApp} from '../../assets';
import {colors} from '../../styles';
import {fonts} from '../../assets';

const {height, width} = Dimensions.get('window');

const SplashScreen = ({navigation}) => {
  console.log(`${height} x ${width}`);
  //ini adalah props bawaan dari navigation container
  useEffect(() => {
    //sama dengan ComponentDidMount di class component
    setTimeout(() => {
      navigation.replace('Welcome'); //.navigate 'back' berfungsi
    }, 3000);
  });
  return (
    <View style={styles.container}>
      <Image source={LogoApp} style={styles.logo}></Image>
      <Text style={styles.textApp}>INFOCOVID19</Text>
      <Text style={styles.textSubApp}>MOBILE APP</Text>
    </View>
  );
};

export default SplashScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.default,
  },
  logo: {
    height: width / 5,
    width: width / 5,
    marginBottom: width / 30,
  },
  textApp: {
    fontSize: width / 14,
    fontWeight: 'bold',
    fontFamily: fonts.default,
    color: colors.text.dark,
  },
  textSubApp: {
    fontSize: width / 25,
    fontWeight: 'bold',
    // fontStyle: 'italic',
    fontFamily: fonts.default,
    color: colors.text.dark,
  },
});

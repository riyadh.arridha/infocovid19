import React, {Component, useState, useContext} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  Dimensions,
  TouchableOpacity,
  TextInput,
  ScrollView,
} from 'react-native';
import {Login} from '../../assets';
import {colors} from '../../styles/colors';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {ButtonDefault} from '../../components';
import {RootContext} from './../../navigation';

const {height, width} = Dimensions.get('window');

const LoginScreen = ({navigation}) => {
  const state = useContext(RootContext);

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isError, setIsError] = useState(false);

  // const handleGoTo = (screen) => {
  //   navigation.navigate(screen);
  // };

  const loginHandler = () => {
    console.log(state.email + ' ' + state.password);
    if ((email === '') & (password === '')) {
      alert('Masukkan email dan password untuk Masuk');
    } else if ((email === state.email) & (password === state.password)) {
      navigation.navigate('Home');
    } else {
      setIsError(true);
    }
  };

  return (
    <View style={styles.container}>
      <ScrollView>
        <View style={styles.top}>
          <View style={styles.buttonBack}>
            <TouchableOpacity
              onPress={() => {
                navigation.goBack();
              }}>
              <Icon name="chevron-left" size={30} color={colors.text.dark} />
            </TouchableOpacity>
          </View>
          <View style={styles.imagePosition}>
            <Image source={Login} style={styles.loginImage} />
          </View>
        </View>

        <View style={styles.bottom}>
          <Text style={styles.loginCaption}>
            Silahkan Masukkan Email dan Password untuk Masuk
          </Text>
          <View style={styles.inputBar}>
            <TextInput
              style={styles.inputText}
              placeholder="Email"
              onChangeText={(textEmail) => setEmail(textEmail)}
            />
          </View>
          {/* <InputLogin title="Your Name" /> */}
          <View style={styles.inputBar}>
            <TextInput
              style={styles.inputText}
              placeholder="Password"
              onChangeText={(textPassword) => setPassword(textPassword)}
              secureTextEntry={true}
            />
          </View>
          {/* <PasswordLogin title="Password" /> */}
          <Text style={isError ? styles.errorText : styles.hiddenErrorText}>
            Email atau Password Salah
          </Text>
          <View style={{alignItems: 'center'}}>
            <ButtonDefault onPress={() => loginHandler()} title="MASUK" />
          </View>
        </View>
      </ScrollView>
    </View>
  );
};
export default LoginScreen;

// const PasswordLogin = ({title}) => {
//   return (
//     <View style={styles.inputBar}>
//       <TextInput
//         style={styles.inputText}
//         placeholder={title}
//         onChangeText={(password) => setPassword({password})}
//         secureTextEntry={true}
//       />
//     </View>
//   );
// };

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.default,
  },
  top: {
    height: '40%',
    width: '100%',
    marginTop: 0,
  },
  buttonBack: {
    marginTop: 25,
    marginLeft: 15,
  },
  loginImage: {
    maxWidth: 177,
    maxHeight: 150,
  },
  imagePosition: {
    alignItems: 'center',
  },

  bottom: {
    flex: 1,
    backgroundColor: 'white',
    height: '60%',
    width: '100%',
    alignItems: 'center',
    borderRadius: 50,
    borderBottomEndRadius: 0,
    borderBottomStartRadius: 0,
    justifyContent: 'space-evenly',
  },

  loginCaption: {
    color: colors.text.dark,
    width: '60%',
    fontSize: 14,
    textAlign: 'center',
    fontWeight: 'bold',
    marginBottom: width / 20,
  },

  inputText: {
    fontSize: 14,
    borderBottomWidth: 1,
    borderColor: colors.text.semi,
  },
  inputBar: {
    width: '65%',
    height: 48,
  },

  buttonTitle: {
    textAlign: 'center',
    color: '#000',
    fontSize: 14,
    fontWeight: 'bold',
  },
  button: {
    backgroundColor: colors.default,
    height: 48,
    width: 232,
    borderRadius: 232 / 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  errorText: {
    color: 'red',
    textAlign: 'center',
    marginBottom: 16,
  },
  hiddenErrorText: {
    color: 'transparent',
    textAlign: 'center',
    marginBottom: 16,
  },
});

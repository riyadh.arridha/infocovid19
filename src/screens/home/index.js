import React, {useContext} from 'react';
import {StyleSheet, View, Text, Dimensions, ScrollView} from 'react-native';
import {colors} from '../../styles';
import {fonts} from '../../assets';
import {Covid, Cegah, Ciri, Video, Info, About} from '../../assets';
import {
  MenuItem,
  InfoBox,
  BackButton,
  Infos,
  InfoButton,
} from '../../components';
import {Positif} from '../../api';
import Recovered from '../../api/sembuh';
import Death from '../../api/meninggal';
import MoreButton from '../../components/atoms/moreButton';
import {RootContext} from './../../navigation';

const {height, width} = Dimensions.get('window');

const HomeScreen = ({navigation}) => {
  const handleGoTo = (screen) => {
    navigation.navigate(screen);
  };
  const state = useContext(RootContext);
  console.log(height + ' , ' + width);
  return (
    <View style={styles.container}>
      {/* <StatusBar /> */}
      <ScrollView>
        <View style={styles.top}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <BackButton
              onPress={() => {
                navigation.goBack();
              }}
            />
            <InfoButton
              onPress={() => {
                handleGoTo('About');
              }}
            />
          </View>
          <Text style={styles.hi}>Hi, {state.name}</Text>
          <Text style={styles.textHeader}>
            Berikut data terbaru mengenai penularan virus Covid-19 secara
            global:
          </Text>
          <View style={styles.infoContainer}>
            <InfoBox title="Positif" content={<Positif />} color="orange" />
            <InfoBox title="Sembuh" content={<Recovered />} color="green" />
            <InfoBox title="Meninggal" content={<Death />} color="red" />
            {/* <InfoBox title="Positif" content={null} color="orange" />
            <InfoBox title="Sembuh" content={null} color="green" />
            <InfoBox title="Meninggal" content={null} color="red" /> */}
          </View>
          {/* <Infos /> */}
          <View style={styles.moreContainer}>
            <MoreButton
              onPress={() => {
                handleGoTo('Info');
              }}
            />
          </View>
        </View>

        <Text style={styles.textMenuCaption}>Jelajahi lebih lanjut</Text>
        <View style={styles.bottom}>
          <View
            style={{
              flexDirection: 'row',
              // backgroundColor: 'blue',
            }}>
            <MenuItem
              name="Covid-19"
              image={Covid}
              onPress={() => {
                handleGoTo('Covid');
              }}
            />
            <MenuItem
              name="Pencegahan"
              image={Cegah}
              onPress={() => {
                handleGoTo('Cegah');
              }}
            />
            <MenuItem
              name="Video"
              image={Video}
              onPress={() => {
                handleGoTo('Video');
              }}
            />
          </View>
          <View style={{flexDirection: 'row'}}>
            <MenuItem
              name="Info"
              image={Info}
              onPress={() => {
                handleGoTo('Info');
              }}
            />
            <MenuItem
              name="Kontak"
              image={Ciri}
              onPress={() => {
                handleGoTo('Kontak');
              }}
            />
            <MenuItem
              name="Akun"
              image={About}
              onPress={() => {
                handleGoTo('Account');
              }}
            />
          </View>
          <View style={{flexDirection: 'row'}}></View>
        </View>
      </ScrollView>
    </View>
  );
};
export default HomeScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  top: {
    backgroundColor: colors.default,
    height: height * 0.42,
    width: width,
    borderBottomEndRadius: 20,
    borderBottomStartRadius: 20,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,

    elevation: 10,
  },
  hi: {
    fontSize: width / 20,
    fontFamily: fonts.default,
    color: colors.text.dark,
    fontWeight: 'bold',
    marginLeft: width / 30,
  },
  textHeader: {
    fontSize: width / 29,
    fontWeight: 'bold',
    fontFamily: fonts.default,
    color: colors.text.dark,
    marginHorizontal: width / 30,
  },
  infoContainer: {
    flexDirection: 'row',
    alignContent: 'center',
    justifyContent: 'space-around',
    // backgroundColor: 'red',
    marginHorizontal: width / 30,
  },
  moreContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    marginTop: width / 40,
    marginRight: width / 30,
  },
  bottom: {
    alignItems: 'center',
  },
  textMenuCaption: {
    fontSize: width / 26,
    fontWeight: 'bold',
    fontFamily: fonts.default,
    color: colors.text.dark,
    textAlign: 'center',
    marginVertical: width / 20,
  },
});

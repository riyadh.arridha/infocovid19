import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Linking,
  Dimensions,
} from 'react-native';
import {colors} from '../../styles';

const {height, width} = Dimensions.get('window');

const Sosmed = ({navigation, links, images, texts}) => {
  return (
    <TouchableOpacity onPress={() => Linking.openURL(links)}>
      <View style={styles.sosmedView}>
        <Image source={images} style={styles.imageStyle}></Image>
        <Text style={styles.bottomText}>{texts}</Text>
      </View>
    </TouchableOpacity>
  );
};
export default Sosmed;

const styles = StyleSheet.create({
  sosmedView: {
    alignItems: 'center',
  },
  imageStyle: {
    height: width / 14,
    width: width / 14,
  },
  bottomText: {
    fontSize: width / 29,
    fontWeight: 'bold',
    color: colors.text.minDark,
    padding: width / 40,
  },
});

import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
  Dimensions,
} from 'react-native';
import {Photo, Facebook, Instagram, Twitter, Gitlab, fonts} from '../../assets';
import Sosmed from './sosmed';
import {colors} from '../../styles';
import {BackButton} from '../../components';

const {height, width} = Dimensions.get('window');

const AboutScreen = ({navigation}) => {
  return (
    <View style={styles.container}>
      <ScrollView>
        <BackButton
          onPress={() => {
            navigation.goBack();
          }}
        />
        <Text style={styles.textHeader}>About Developer</Text>
        <View style={styles.top}>
          <Image source={Photo} style={styles.photo}></Image>
          <Text style={styles.nameText}>Riyadh Arridha</Text>
        </View>

        <View style={styles.bottom}>
          <View style={styles.bottomView}>
            <Sosmed
              images={Facebook}
              texts="@riyadh.arridha"
              links="https://www.facebook.com/riyadh.arridha/"
            />
            <Sosmed
              images={Instagram}
              texts="@riyadh.arridha"
              links="https://www.instagram.com/riyadh.arridha/"
            />
            <Sosmed
              images={Twitter}
              texts="@riyadh_arridha"
              links="https://www.twitter.com/riyadh_arridha/"
            />
            <Text style={styles.caption}>Social Media Account</Text>
            <Sosmed
              images={Gitlab}
              texts="gitlab/@riyadh.arridha"
              links="https://www.gitlab.com/riyadh.arridha/"
            />
            <Text style={styles.caption}>My Awesome Project</Text>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};
export default AboutScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.default,
  },
  //atas
  top: {
    height: height * 0.27,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.default,
  },
  textHeader: {
    fontSize: width / 29,
    fontWeight: 'bold',
    color: colors.text.dark,
    marginHorizontal: width / 30,
  },
  photo: {
    height: height / 5.6,
    width: width / 4.6,
  },
  nameText: {
    fontSize: width / 20,
    color: colors.text.dark,
    fontWeight: 'bold',
    padding: width / 40,
  },
  //bawah
  bottom: {
    // flex: 1,
    height: height * 0.65,
    alignItems: 'center',
    borderTopStartRadius: 20,
    borderTopEndRadius: 20,
    backgroundColor: '#FFF',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,

    elevation: 10,
  },
  caption: {
    fontSize: width / 23,
    color: colors.default,
    fontWeight: 'bold',
    marginBottom: width / 20,
    padding: width / 50,
  },
  bottomView: {
    marginTop: width / 15,
  },
});

import React, {useContext, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
  Dimensions,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import {Photo} from '../../assets';
import {colors} from '../../styles';
import {BackButton, ButtonDefault} from '../../components';
import Icon from 'react-native-vector-icons/MaterialIcons';

const {height, width} = Dimensions.get('window');

import {RootContext} from './../../navigation';

const Account = ({navigation}) => {
  const state = useContext(RootContext);

  const [name, setName] = useState(state.name);
  const [password, setPassword] = useState(state.password);
  const [confirmPassword, setConfirmPassword] = useState(state.confirmPassword);
  const [isError, setIsError] = useState(false);

  const handleSavePress = () => {
    if ((name === '') | (confirmPassword === '') | (password === '')) {
      alert('Data tidak boleh kosong');
    } else {
      if (password === confirmPassword) {
        state.handleNameChange(name);
        state.handlePasswordChange(password);
        state.handleConfirmPasswordChange(confirmPassword);
        alert('Berhasil menyimpan perubahan');
      } else {
        setIsError(true);
      }
    }
  };

  return (
    <View style={styles.container}>
      <ScrollView>
        <BackButton
          onPress={() => {
            navigation.goBack();
          }}
        />
        <View style={styles.top}>
          {/* ganti photo dengan image utk ganti photo */}
          <View style={{position: 'relative'}}>
            <Image source={Photo} style={styles.photo} />
            <TouchableOpacity
              style={{position: 'absolute', top: 0, right: 0}}
              onPress={() => alert('Browse for photo?')}>
              <Icon
                name="add-a-photo"
                size={width / 20}
                color={colors.text.default}
              />
            </TouchableOpacity>
          </View>
          <Text style={styles.nameText}>{state.name}</Text>
        </View>

        <View style={styles.bottom}>
          <View style={styles.bottomView}>
            <Text style={styles.caption}>Detail Account</Text>
            <Text style={styles.inputTitle}>Name</Text>
            <TextInput
              value={name}
              style={styles.inputText}
              editable={true}
              onChangeText={(textUser) => setName(textUser)}
            />
            <Text style={styles.inputTitle}>Email</Text>
            <TextInput
              value={state.email}
              editable={false}
              style={styles.inputText}
            />
            <Text style={styles.inputTitle}>Password</Text>
            <TextInput
              value={password}
              secureTextEntry={true}
              style={styles.inputText}
              onChangeText={(textPassword) => setPassword(textPassword)}
            />
            <Text style={styles.inputTitle}>Konfirmasi Password</Text>
            <TextInput
              value={confirmPassword}
              secureTextEntry={true}
              style={styles.inputText}
              onChangeText={(textConfirmPassword) =>
                setConfirmPassword(textConfirmPassword)
              }
            />
            <Text style={isError ? styles.errorText : styles.hiddenErrorText}>
              Password Tidak Sama
            </Text>

            {/* masih bisa d tambahkan untuk data lainnya */}

            <ButtonDefault
              onPress={() => handleSavePress()}
              title="SIMPAN PERUBAHAN"
            />
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

export default Account;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.default,
  },
  //atas
  top: {
    height: height * 0.27,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.default,
  },
  photo: {
    height: width / 4.6,
    width: width / 4.6,
    borderRadius: width / 4.6 / 2,
  },
  nameText: {
    fontSize: width / 20,
    color: colors.text.dark,
    fontWeight: 'bold',
    padding: width / 40,
  },
  //bawah
  bottom: {
    flex: 1,
    // height: height * 0.75,
    alignItems: 'center',
    borderTopStartRadius: 20,
    borderTopEndRadius: 20,
    backgroundColor: '#FFF',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,

    elevation: 10,
  },
  caption: {
    fontSize: width / 23,
    color: colors.default,
    fontWeight: 'bold',
    marginBottom: width / 20,
    padding: width / 50,
    textAlign: 'center',
  },
  bottomView: {
    marginTop: width / 15,
  },
  inputTitle: {
    fontWeight: 'bold',
    paddingTop: width / 40,
    color: colors.text.dark,
  },
  inputText: {
    borderColor: colors.text.dark,
    borderBottomWidth: 1,
    fontSize: width / 34,
  },
  errorText: {
    color: 'red',
    textAlign: 'center',
    marginBottom: 16,
  },
  hiddenErrorText: {
    color: 'transparent',
    textAlign: 'center',
    marginBottom: 16,
  },
});

import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  SafeAreaView,
  Linking,
} from 'react-native';
import {fonts} from './../../utils/fonts';
import {colors} from './../../utils/colors';
import {Welcome} from '../../assets';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const {height, width} = Dimensions.get('window');

export default class CovidPage extends Component {
  render() {
    const {navigation} = this.props;
    return (
      <View style={styles.container}>
        {/* <StatusBar /> */}

        <View style={styles.top}>
          <View style={styles.buttonBack}>
            <TouchableOpacity
              onPress={() => {
                navigation.goBack();
              }}>
              <Icon name="chevron-left" size={30} color={colors.text.dark} />
            </TouchableOpacity>
          </View>

          <Text style={styles.hi}>Hi, User</Text>
          <Text style={styles.textHeader}>
            Berikut informasi mengenai Covid-19
          </Text>
        </View>

        <SafeAreaView style={styles.bottom}>
          <ScrollView>
            <Text style={styles.paragraph}>
              Pandemi Koronavirus 2019–2020 atau dikenal sebagai pandemi
              COVID-19 adalah peristiwa menyebarnya penyakit koronavirus 2019,{' '}
              <Text
                style={{color: 'blue'}}
                onPress={() => Linking.openURL('http://google.com')}>
                Google
              </Text>
              (bahasa Inggris: coronavirus disease 2019, disingkat COVID-19) di
              seluruh dunia. Penyakit ini disebabkan oleh koronavirus jenis baru
              yang diberi nama SARS-CoV-2.[1] {'\n'}Wabah COVID-19 pertama kali
              dideteksi di Kota Wuhan, Provinsi Hubei, Tiongkok pada bulan
              Desember 2019, dan ditetapkan sebagai pandemi oleh Organisasi
              Kesehatan Dunia (WHO) pada 11 Maret 2020.[2] {'\n'}Hingga 23 April
              2020, lebih dari 2.000.000 kasus COVID-19 telah dilaporkan di
              lebih dari 210 negara dan wilayah, mengakibatkan lebih dari
              195,755 orang meninggal dunia dan lebih dari 781,109 orang sembuh.
              {'\n'}Pandemi Koronavirus 2019–2020 atau dikenal sebagai pandemi
              COVID-19 adalah peristiwa menyebarnya penyakit koronavirus 2019
              (bahasa Inggris: coronavirus disease 2019, disingkat COVID-19) di
              seluruh dunia. Penyakit ini disebabkan oleh koronavirus jenis baru
              yang diberi nama SARS-CoV-2.[1] {'\n'}Wabah COVID-19 pertama kali
              dideteksi di Kota Wuhan, Provinsi Hubei, Tiongkok pada bulan
              Desember 2019, dan ditetapkan sebagai pandemi oleh Organisasi
              Kesehatan Dunia (WHO) pada 11 Maret 2020.[2] {'\n'}Hingga 23 April
              2020, lebih dari 2.000.000 kasus COVID-19 telah dilaporkan di
              lebih dari 210 negara dan wilayah, mengakibatkan lebih dari
              195,755 orang meninggal dunia dan lebih dari 781,109 orang sembuh.
            </Text>
          </ScrollView>
        </SafeAreaView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: "center",
    // alignItems: "center",
    backgroundColor: colors.default,
  },
  top: {
    backgroundColor: colors.default,
    height: '20%',
    width: '100%',
  },
  buttonBack: {
    marginTop: 25,
    marginLeft: 15,
  },
  hi: {
    fontSize: 24,
    fontFamily: fonts.default,
    color: 'transparent',
    fontWeight: 'bold',
    marginLeft: 15,
  },
  textHeader: {
    fontSize: 14,
    fontWeight: 'bold',
    fontFamily: fonts.default,
    color: colors.text.dark,
    // textAlign: "center",
    marginHorizontal: 15,
  },
  bottom: {
    // height: "25%",
    flex: 1,
    width: '100%',
    marginBottom: 0,
    backgroundColor: '#FFF',
    borderTopLeftRadius: 50,
    borderTopRightRadius: 50,
    paddingTop: 20,
    // alignItems: "center",
  },
  paragraph: {
    fontSize: 16,
    fontWeight: 'normal',
    fontFamily: fonts.default,
    color: colors.text.dark,
    textAlign: 'justify',
    margin: 25,
    lineHeight: 20,
  },
  next: {
    textAlign: 'right',
    marginRight: 20,
  },
});

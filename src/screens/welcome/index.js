import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {colors} from '../../styles/colors';
import {fonts} from '../../assets/fonts';
import {Welcome} from '../../assets';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const {height, width} = Dimensions.get('window');

const handleGoTo = (navigation, screen) => {
  navigation.replace(screen);
};

const WelcomeScreen = ({navigation}) => {
  return (
    <View style={styles.container}>
      {/* <StatusBar /> */}
      <View style={styles.header}>
        <Image source={Welcome} style={styles.imageSetup} />
        <Text style={styles.textHeader}>
          Mari bersama-sama mencegah penularan Covid-19 dengan mematuhi protokol
          kesehatan yang telah ditetapkan
        </Text>
      </View>
      <View style={styles.footer}>
        <Text style={styles.textFooter}>
          Ingin tahu lebih banyak mengenai Covid-19?
        </Text>
        <TouchableOpacity
          onPress={() => {
            handleGoTo(navigation, 'Register');
          }}>
          <Icon
            name="arrow-right"
            color={colors.text.dark}
            size={width / 12.8}
            style={styles.next}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default WelcomeScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.default,
  },
  header: {
    backgroundColor: '#FFF',
    height: height * 0.8,
    width: width,
    marginTop: 0,
    borderBottomEndRadius: 20,
    borderBottomStartRadius: 20,
    justifyContent: 'space-around',
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,

    elevation: 10,
  },
  imageSetup: {
    maxWidth: width * 0.8,
    maxHeight: height * 0.7,
  },
  textHeader: {
    fontSize: width / 30,
    fontWeight: 'bold',
    fontFamily: fonts.default,
    color: colors.text.dark,
    textAlign: 'center',
  },
  footer: {
    flex: 1,
    justifyContent: 'space-evenly',
  },
  textFooter: {
    fontSize: width / 23,
    fontWeight: 'bold',
    fontFamily: fonts.default,
    color: colors.text.dark,
    textAlign: 'center',
    paddingHorizontal: width / 15,
    paddingTop: width / 40,
  },
  next: {
    textAlign: 'right',
    marginRight: width / 20,
  },
});

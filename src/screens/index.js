import SplashScreen from './splash';
import WelcomeScreen from './welcome';
import HomeScreen from './home';
import AboutScreen from './about';
import AccountScreen from './account';
import LoginScreen from './login';
import InfoScreen from './info';
import RegisterScreen from './register';

export {
  SplashScreen,
  WelcomeScreen,
  HomeScreen,
  AboutScreen,
  AccountScreen,
  LoginScreen,
  InfoScreen,
  RegisterScreen,
};

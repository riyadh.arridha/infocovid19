import React from 'react';
import {View, StyleSheet, Dimensions} from 'react-native';
import {Positif, Recovered, Death} from '../../../api';
import InfoBox from '../../molecules/infoBox';
import MoreButton from '../../atoms/moreButton';

const {height, width} = Dimensions.get('window');

const Infos = () => {
  return (
    <View style={styles.infoContainer}>
      <InfoBox title="Positif" content={<Positif />} color="orange" />
      <InfoBox title="Sembuh" content={<Recovered />} color="green" />
      <InfoBox title="Meninggal" content={<Death />} color="red" />
    </View>
  );
};

export default Infos;

const styles = StyleSheet.create({
  infoContainer: {
    flexDirection: 'row',
    alignContent: 'center',
    justifyContent: 'space-around',
  },
  moreContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    marginTop: width / 40,
  },
});

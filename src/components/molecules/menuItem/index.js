import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {colors} from '../../../styles';

const {height, width} = Dimensions.get('window');

const MenuItem = ({image, name, onPress}) => {
  // console.log(height + ',' + width);
  const handleGoTo = (screen) => {
    navigation.navigate(screen);
  };
  return (
    <View style={styles.menuContainer}>
      <TouchableOpacity onPress={onPress}>
        <View style={styles.menuView}>
          <Image
            source={image}
            style={{height: width / 10, width: width / 10}}
          />
        </View>
        <Text style={styles.textMenu}>{name}</Text>
      </TouchableOpacity>
    </View>
  );
};

export default MenuItem;

const styles = StyleSheet.create({
  menuContainer: {
    height: width / 4,
    width: width / 4,
    marginHorizontal: width / 40,
    marginVertical: width / 40,
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: 'red',
  },
  menuView: {
    height: width / 6,
    width: width / 6,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 15,
    backgroundColor: colors.default,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,

    elevation: 10,
  },
  textMenu: {
    fontSize: width / 34,
    fontWeight: 'bold',
    textAlign: 'center',
    paddingVertical: width / 60,
    color: colors.text.dark,
  },
});

import React from 'react';
import {StyleSheet, View, Text, Dimensions} from 'react-native';
import {colors} from '../../../styles';

const {height, width} = Dimensions.get('window');

const InfoBox = ({color, title, content}) => {
  return (
    <View style={[styles.infoView, {backgroundColor: color}]}>
      <Text style={styles.infoTitle}>{title}</Text>
      <Text style={styles.infoCaption}>{content}</Text>
    </View>
  );
};

export default InfoBox;

const styles = StyleSheet.create({
  infoView: {
    height: width / 4,
    width: width / 3.5,
    // marginHorizontal: width / 41,
    marginVertical: width / 41,
    justifyContent: 'space-around',
    alignItems: 'center',
    borderRadius: 20,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,

    elevation: 10,
  },
  infoTitle: {
    fontWeight: 'bold',
    fontSize: width / 30,
    color: colors.text.dark,
  },
  infoCaption: {
    fontSize: width / 28,
    fontWeight: 'bold',
    color: colors.text.dark,
  },
});

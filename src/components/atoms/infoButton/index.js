import React from 'react';
import {TouchableOpacity, StyleSheet, Dimensions} from 'react-native';
import {View} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Icon1 from 'react-native-vector-icons/MaterialCommunityIcons';

import {colors} from '../../../styles';

const {height, width} = Dimensions.get('window');
const InfoButton = ({onPress}) => {
  return (
    <View style={styles.buttonBack}>
      <TouchableOpacity onPress={onPress}>
        <Icon1
          // name="ios-information"
          name="information-outline"
          size={width / 14}
          color={colors.text.dark}
        />
      </TouchableOpacity>
    </View>
  );
};

export default InfoButton;

const styles = StyleSheet.create({
  buttonBack: {
    marginTop: width / 30,
    marginRight: width / 30,
  },
});

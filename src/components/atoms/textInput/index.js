import React from 'react';
import {TextInput, View, StyleSheet} from 'react-native';
import {colors} from '../../../styles';

const TextInputDefault = (placeholder, onChangeText) => {
  return (
    <View style={styles.inputBar}>
      <TextInput
        style={styles.inputText}
        placeholder={placeholder}
        onChangeText={onChangeText}
        secureTextEntry={true}
      />
    </View>
  );
};
export default TextInputDefault;

const styles = StyleSheet.create({
  inputText: {
    fontSize: 14,
    borderBottomWidth: 1,
    borderColor: colors.text.semiDark,
  },
  inputBar: {
    width: '65%',
    height: 48,
  },
});

import React from 'react';
import {TouchableOpacity, StyleSheet, Dimensions} from 'react-native';
import {View} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {colors} from '../../../styles';

const {height, width} = Dimensions.get('window');
const BackButton = ({onPress}) => {
  return (
    <View style={styles.buttonBack}>
      <TouchableOpacity onPress={onPress}>
        <Icon name="chevron-left" size={width / 14} color={colors.text.dark} />
      </TouchableOpacity>
    </View>
  );
};

export default BackButton;

const styles = StyleSheet.create({
  buttonBack: {
    marginTop: width / 30,
    // marginLeft: width / 30,
  },
});

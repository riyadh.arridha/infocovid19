import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import {colors} from '../../../styles';

const {width} = Dimensions.get('window');
const Button = ({onPress, title}) => {
  return (
    <View style={{alignItems: 'center', marginVertical: width / 10}}>
      <TouchableOpacity onPress={onPress}>
        <View style={styles.button}>
          <Text style={styles.buttonTitle}>{title}</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default Button;

const styles = StyleSheet.create({
  buttonTitle: {
    textAlign: 'center',
    color: '#000',
    fontSize: 14,
    fontWeight: 'bold',
  },
  button: {
    backgroundColor: colors.default,
    height: 48,
    width: 232,
    borderRadius: 232 / 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

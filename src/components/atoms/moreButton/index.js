import React from 'react';
import {
  TouchableOpacity,
  View,
  Text,
  StyleSheet,
  Dimensions,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {colors} from '../../../styles';

const {height, width} = Dimensions.get('window');

const MoreButton = ({onPress}) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <View style={styles.moreView}>
        <Text style={styles.moreText}>Selengkapnya</Text>
        <Icon name="arrow-right" size={width / 20} style={styles.icon} />
      </View>
    </TouchableOpacity>
  );
};
export default MoreButton;

const styles = StyleSheet.create({
  icon: {
    // marginRight: width / 30,
    color: colors.text.dark,
  },
  moreView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  moreText: {
    fontSize: width / 24,
    fontWeight: 'bold',
    color: colors.text.dark,
  },
});
